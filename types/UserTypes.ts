import { Nullable } from '~/types/Nullable'

export enum UserRole {
  ADMIN = 'admin',
  CUSTOMER = 'customer',
}

export interface IUser {
  id: number
  email: string
  password: string
  name: string
  role: UserRole
  avatar: string
  creationAt: string
  updatedAt: string
}

export interface IUserState {
  user: Nullable<IUser>
  token: string
  isError: boolean
  isLoading: boolean
}

export interface ILoginForm {
  email: string
  password: string
}
