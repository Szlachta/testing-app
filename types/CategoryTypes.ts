export interface ICategory {
  id: number
  name: string
  image: string
  creationAt: string
  updatedAt: string
}

export interface ICategoryState {
  categories: ICategory[]
  isError: boolean
  isLoading: boolean
}
