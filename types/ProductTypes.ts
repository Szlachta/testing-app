import { ICategory } from '~/types/CategoryTypes'
import { Nullable } from '~/types/Nullable'

export interface IProduct {
  id: number
  title: string
  price: number
  description: string
  images: string[]
  creationAt: string
  updatedAt: string
  category: ICategory
}

export type IProductForm = {
  title: string
  price: number
  description: string
  images: string[]
  categoryId: number
}

export interface IProductState {
  isError: boolean
  isLoading: boolean
  products: IProduct[]
  product: Nullable<IProduct>
}

export interface IProductPayload {
  id: number
  form: IProductForm
}

export interface IPaginationPayload {
  limit?: number
  offset: number
}
