import common from './common'
import errors from './errors'
import forms from './forms'

export default {
  ...common,
  ...errors,
  ...forms,
}
