import { VuexModule, Module, Mutation, Action } from 'vuex-class-modules'
import { store } from '~/store/store'
import { ILoginForm, IUser, IUserState } from '~/types/UserTypes'
import UserRepository from '~/repositories/UserRepository'
import { Nullable } from '~/types/Nullable'
import cookieHelper from '~/helpers/cookieHelper'

@Module
class UserService extends VuexModule {
  public stateUser: IUserState = {
    isError: false,
    isLoading: true,
    user: null,
    token: '',
  }

  @Mutation
  public setIsError(isError: boolean) {
    this.stateUser.isError = isError
  }

  @Mutation
  public setIsLoading(isLoading: boolean) {
    this.stateUser.isLoading = isLoading
  }

  @Mutation
  public setUser(user: Nullable<IUser>) {
    this.stateUser.user = user
  }

  @Mutation
  public setToken(token: string) {
    cookieHelper.setCookie('token', token, 1)
    this.stateUser.token = token
  }

  public get isError(): boolean {
    return this.stateUser.isError
  }

  public get isLoading(): boolean {
    return this.stateUser.isLoading
  }

  public get user(): Nullable<IUser> {
    return this.stateUser.user
  }

  public get token(): string {
    if (!this.stateUser.token) {
      this.stateUser.token = cookieHelper.getCookie('token') ?? ''
    }
    return this.stateUser.token
  }

  @Action
  public async login(form: ILoginForm): Promise<void> {
    this.setIsLoading(true)

    await UserRepository.login(form)
      .then((token) => {
        this.setToken(token)
        this.setIsError(false)
      })
      .catch(() => {
        this.setToken('')
        this.setIsError(true)
      })
      .finally(() => {
        this.setIsLoading(false)
      })
  }

  @Action
  public logout(): void {
    this.setToken('')
  }
}

const userService = new UserService({ store, name: 'userService' })
export default userService
