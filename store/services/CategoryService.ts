import { VuexModule, Module, Mutation, Action } from 'vuex-class-modules'
import { store } from '~/store/store'
import { ICategory, ICategoryState } from '~/types/CategoryTypes'
import CategoryRepository from '~/repositories/CategoryRepository'

@Module
class CategoryService extends VuexModule {
  public stateCategory: ICategoryState = {
    isError: false,
    isLoading: true,
    categories: [],
  }

  @Mutation
  public setIsError(isError: boolean) {
    this.stateCategory.isError = isError
  }

  @Mutation
  public setIsLoading(isLoading: boolean) {
    this.stateCategory.isLoading = isLoading
  }

  @Mutation
  public setCategories(categories: ICategory[]) {
    this.stateCategory.categories = categories
  }

  public get isError(): boolean {
    return this.stateCategory.isError
  }

  public get isLoading(): boolean {
    return this.stateCategory.isLoading
  }

  public get categories(): ICategory[] {
    return this.stateCategory.categories
  }

  @Action
  public async loadCategories(): Promise<void> {
    this.setIsLoading(true)

    await CategoryRepository.categories()
      .then((categories) => {
        this.setCategories(categories)
        this.setIsError(false)
      })
      .catch(() => {
        this.setCategories([])
        this.setIsError(true)
      })
      .finally(() => {
        this.setIsLoading(false)
      })
  }
}

const categoryService = new CategoryService({ store, name: 'categoryService' })
export default categoryService
