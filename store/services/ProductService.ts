import { VuexModule, Module, Mutation, Action } from 'vuex-class-modules'
import { store } from '~/store/store'
import {
  IPaginationPayload,
  IProduct,
  IProductForm,
  IProductPayload,
  IProductState,
} from '~/types/ProductTypes'
import ProductRepository from '~/repositories/ProductRepository'
import { Nullable } from '~/types/Nullable'

@Module
class ProductService extends VuexModule {
  public stateProduct: IProductState = {
    isError: false,
    isLoading: true,
    products: [],
    product: null,
  }

  @Mutation
  public setIsError(isError: boolean) {
    this.stateProduct.isError = isError
  }

  @Mutation
  public setIsLoading(isLoading: boolean) {
    this.stateProduct.isLoading = isLoading
  }

  @Mutation
  public setProducts(products: IProduct[]) {
    this.stateProduct.products = products
  }

  @Mutation
  public setProduct(product: Nullable<IProduct>) {
    this.stateProduct.product = product
  }

  public get isError(): boolean {
    return this.stateProduct.isError
  }

  public get isLoading(): boolean {
    return this.stateProduct.isLoading
  }

  public get products(): IProduct[] {
    return this.stateProduct.products
  }

  public get product(): Nullable<IProduct> {
    return this.stateProduct.product
  }

  @Action
  public async loadProducts(): Promise<void> {
    this.setIsLoading(true)

    await ProductRepository.products()
      .then((products) => {
        this.setProducts(products)
        this.setIsError(false)
      })
      .catch(() => {
        this.setIsError(true)
      })
      .finally(() => {
        this.setIsLoading(false)
      })
  }

  @Action
  public async loadProductsPagination(
    paginationPayload: IPaginationPayload
  ): Promise<void> {
    this.setIsLoading(true)

    await ProductRepository.products(
      paginationPayload.limit,
      paginationPayload.offset
    )
      .then((products) => {
        this.setProducts(products)
        this.setIsError(false)
      })
      .catch(() => {
        this.setIsError(true)
      })
      .finally(() => {
        this.setIsLoading(false)
      })
  }

  @Action
  public async loadProduct(id: number): Promise<void> {
    this.setIsLoading(true)

    await ProductRepository.product(id)
      .then((product) => {
        this.setProduct(product)
        this.setIsError(false)
      })
      .catch(() => {
        this.setIsError(true)
      })
      .finally(() => {
        this.setIsLoading(false)
      })
  }

  @Action
  public async create(productForm: IProductForm): Promise<void> {
    this.setIsLoading(true)

    await ProductRepository.createProduct(productForm)
      .then((product) => {
        this.setProduct(product)
        this.setIsError(false)
      })
      .catch(() => {
        this.setIsError(true)
      })
      .finally(() => {
        this.setIsLoading(false)
      })
  }

  @Action
  public async update(payload: IProductPayload): Promise<void> {
    this.setIsLoading(true)

    await ProductRepository.updateProduct(payload.id, payload.form)
      .then((product) => {
        this.setProduct(product)
        this.setIsError(false)
      })
      .catch(() => {
        this.setIsError(true)
      })
      .finally(() => {
        this.setIsLoading(false)
      })
  }

  @Action
  public async delete(productId: number): Promise<void> {
    this.setIsLoading(true)

    await ProductRepository.deleteProduct(productId)
      .then(() => {
        this.setProduct(null)
        this.setIsError(false)
      })
      .catch(() => {
        this.setIsError(true)
      })
      .finally(() => {
        this.setIsLoading(false)
      })
  }
}

const productService = new ProductService({ store, name: 'productService' })
export default productService
