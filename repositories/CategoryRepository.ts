import axios from 'axios'
import UrlHelper from '~/helpers/urls'
import { ICategory } from '~/types/CategoryTypes'

class CategoryRepository {
  public async categories(): Promise<ICategory[]> {
    return await axios
      .get(UrlHelper.getFullApiUrl(`categories`))
      .then((response) => {
        return response.data
      })
      .catch((error) => {
        throw error
      })
  }
}

export default new CategoryRepository()
