import axios from 'axios'
import UrlHelper from '~/helpers/urls'
import { ILoginForm } from '~/types/UserTypes'

class UserRepository {
  public async login(loginData: ILoginForm): Promise<string> {
    return await axios
      .post(UrlHelper.getFullApiUrl('auth/login'), loginData)
      .then((response) => {
        return response.data.access_token
      })
      .catch((error) => {
        throw error
      })
  }

  public async refresh(): Promise<boolean> {
    return await axios
      .post(UrlHelper.getFullApiUrl('auth/refresh-token'))
      .then(() => {
        return true
      })
      .catch((error) => {
        throw error
      })
  }
}

export default new UserRepository()
