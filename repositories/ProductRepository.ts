import axios from 'axios'
import UrlHelper from '~/helpers/urls'
import { IProduct, IProductForm } from '~/types/ProductTypes'

class ProductRepository {
  public async products(
    limit: number = 10,
    offset: number = 0
  ): Promise<IProduct[]> {
    return await axios
      .get(
        UrlHelper.getFullApiUrl(`products?limit=${limit}&offset=${offset}`),
        {
          headers: UrlHelper.getHeader(),
        }
      )
      .then((response) => {
        return response.data
      })
      .catch((error) => {
        throw error
      })
  }

  public async product(id: number): Promise<IProduct> {
    return await axios
      .get(UrlHelper.getFullApiUrl(`products/${id}`), {
        headers: UrlHelper.getHeader(),
      })
      .then((response) => {
        return response.data
      })
      .catch((error) => {
        throw error
      })
  }

  public async createProduct(product: IProductForm): Promise<IProduct> {
    return await axios
      .post(UrlHelper.getFullApiUrl(`products`), product, {
        headers: UrlHelper.getHeader(),
      })
      .then((response) => {
        return response.data
      })
      .catch((error) => {
        throw error
      })
  }

  public async updateProduct(
    id: number,
    product: IProductForm
  ): Promise<IProduct> {
    return await axios
      .put(UrlHelper.getFullApiUrl(`products/${id}`), product, {
        headers: UrlHelper.getHeader(),
      })
      .then((response) => {
        return response.data
      })
      .catch((error) => {
        throw error
      })
  }

  public async deleteProduct(id: number): Promise<boolean> {
    return await axios
      .delete(UrlHelper.getFullApiUrl(`products/${id}`), {
        headers: UrlHelper.getHeader(),
      })
      .then(() => {
        return true
      })
      .catch((error) => {
        throw error
      })
  }
}

export default new ProductRepository()
