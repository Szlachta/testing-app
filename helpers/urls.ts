import userService from '~/store/services/UserService'

export default class UrlHelper {
  private static API_DEV_ADDRESS = '/api/v1/'

  public getFullUrl(path: string): string {
    let port = ''
    if (window.location.hostname.search('localhost') > -1) {
      port = ':3000'
    }
    return (
      window.location.protocol +
      '//' +
      window.location.hostname +
      port +
      '/' +
      path
    )
  }

  public getFullFrontUrl(path: string): string {
    let port = ''
    if (window.location.hostname.search('localhost') > -1) {
      port = ':3000'
    }
    return (
      window.location.protocol +
      '//' +
      window.location.hostname +
      port +
      '/' +
      path
    )
  }

  public static getFullApiUrl(path: string): string {
    return this.API_DEV_ADDRESS + path
  }

  public static getHeader(): Record<string, string> {
    return { Authorization: `Bearer ${userService.token}` }
  }
}
